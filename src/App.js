import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

class App extends Component {

    constructor (props){
        super(props);

        this.state = {
            id: '',
            newName: '',
            newEmail: '',
            isEditEnable : false,
            formErrors: {newName: '', newEmail: ''},
            users : [
                {id: 1, name: 'arif', email: 'arif@live.com'},
                {id: 2, name: 'naveed', email: 'naveed@live.com'},
                {id: 3, name: 'Shahid', email: 'Shahid@live.com'},
                {id: 4, name: 'Akber', email: 'Akber@live.com'}
            ]
        };

    }

    /**
     * Reset Form
     */
    resetForm = () => {
        this.setState({
            id: '',
            newName: '',
            newEmail: ''
        });
    };

    /**
     * Add new user
     */
    addUser = () => {
        let id = this.state.users.length + 1;
        let data = {id: id, name: this.state.newName, email: this.state.newEmail};
        let newUser = this.state.users;
        newUser.unshift(data);
        this.setState({users: newUser});
        this.resetForm();
    };

    /**
     * Update User
     * @param event
     */
    updateUser = (event) => {
        const list = this.state.users.map((item, index) => {
            if(item.id === this.state.id){
                let newUser = this.state.users;
                newUser[index].name = this.state.newName;
                newUser[index].email = this.state.newEmail;
                this.setState({users: newUser});

                this.resetForm();
            }
        });
    };

    handleValidation = () =>{
        let errors = {};
        let formIsValid = true;

        //Name
        if(!this.state.newName){
            formIsValid = false;
            errors["newName"] = "Name is required field";
        }

        if(this.state.newName && !this.state.newName.match(/^[a-zA-Z]+$/)){
            formIsValid = false;
            errors["newName"] = 'Only letters';
        }

        //Email
        if(!this.state.newEmail){
            formIsValid = false;
            errors["newEmail"] = 'Email is required field';
        }

        this.setState({formErrors: errors});
        return formIsValid;
    };

    /**
     *  submit form handling
     * @param event
     */
    handleSubmit = (event) => {
        event.preventDefault();

        if(!this.handleValidation()){
            return false;
        }

        this.setState({isEditEnable: false});

        if(this.state.isEditEnable){
            this.updateUser(event);
            return
        }

        this.addUser(event);
    };

    /**
     * Set user detail into form
     * @param id
     * @param index
     */
    editUser = (id, index) => {

        this.setState({formErrors: {}});

        this.setState({isEditEnable: true});
        let data  = this.state.users[index];
        this.setState({
            id: id,
            newName: data.name,
            newEmail: data.email
        })
    };

    /**
     * Delete User
     * @param id
     * @param index
     */
    deleteUser = (id, index) => {
        this.state.users.splice(index,1);
        this.setState({
            users: this.state.users.filter(i => i !== index)
        });
    };

    render() {
        return(
            <div className='col pt-5 pb-5'>
                <h3 className={'text-center'}>Add New Student</h3>
                <form className="needs-validation" onSubmit={this.handleSubmit} noValidate>
                    <input type="hidden" className="form-control"
                           value={this.state.id}
                           onChange={(event) => this.setState({id: event.target.value})}/>
                    <div className="form-group">
                        <label>Name:  </label>
                        <input type="text" className="form-control"
                               ref={'newName'}
                               value={this.state.newName}
                               onChange={(event) => this.setState({newName: event.target.value})}/>
                        {this.state.formErrors["newName"] && <span className={'error-msg'}>{this.state.formErrors["newName"]}</span>}
                    </div>
                    <div className="form-group">
                        <label>Email: </label>
                        <input type="text"
                               className="form-control"
                               ref='newEmail'
                               value={this.state.newEmail}
                               onChange={(event) => this.setState({newEmail: event.target.value})}/>
                        {this.state.formErrors["newEmail"] && <span className={'error-msg'}>{this.state.formErrors["newEmail"]}</span>}
                    </div>
                    <button className='btn btn-primary' type={'submit'}>{(this.state.isEditEnable) ? 'Update': 'Submit'}</button>
                </form>

                <h3 className={'text-center'}>Students List</h3>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                    <thead>
                    <tr>
                        <th width="200">Name</th>
                        <th width="200">Email</th>
                        <th width="200">action</th>
                    </tr>
                    </thead>
                    {
                        this.state.users.map((user, index) =>
                            <tbody key={user.id}>
                            <tr>
                                <td>{user.name}</td>
                                <td>{user.email}</td>
                                <td>
                                    <button className='float-right mr-2 btn btn-danger btn-sm'
                                            onClick={() => this.deleteUser(user.id, index)}>delete</button>
                                    <button className='float-right mr-2 btn btn-info btn-sm'
                                            onClick={() => this.editUser(user.id, index)}>edit</button>
                                </td>
                            </tr>
                            </tbody>
                        )
                    }
                </table>
            </div>
        )
    }
}

export default App;